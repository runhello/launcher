# launcher

A version of Polyconsole with the ability to switch between multiple game paks. Intended as a workshop submission, so not terribly generalized.

**This repository has [moved](https://github.com/mcclure/bitbucket-backup/tree/archive/repos/launcher).**
